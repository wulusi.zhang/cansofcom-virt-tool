# CANSOFCOM - Video Information Refinement Tool

The VIRT tool is a web-based Internet-of-Things powered tool made in Node-RED, its primary usage is to filter through databases containing files processed by a video tagging Machine Learning model. This repository contains all working files needed to deploy VIRT, and documentation to further develop the software

## Installing

VIRT is built using Node-RED, JQuery, and SASS. However, only Node-RED needs to be installed and configured correctly. The steps are:

- [Install Node-RED onto the target platform](https://nodered.org/docs/getting-started/)
- Clone/Download this entire repository
- Run Node-RED with the `-u` option and [passing the local directory as the location](https://nodered.org/docs/getting-started/running#command-line-usage)
  Example: if the repository was downloaded to `C:/virt-tool` then run `node-red -u "C:/virt-tool"` to start VIRT
- Navigate to the URL specified by Node-RED to access the flow editor of VIRT
- To access the tool itself, navigate to the flow URL followed by `/virt-tool`
  Example: If the flow editor URL is `120.0.0.1:1880`, then to access the tool navigate to `120.0.0.1:1880/virt-tool`

## Developing

The following programs need to be installed to maintain and add features to VIRT:

- [Node-RED](https://nodered.org/) and [Node.JS](https://nodejs.org/en/)
- [SASS](https://sass-lang.com/) ([dart-sass](https://github.com/sass/dart-sass) is recommended)

To learn more about the specific parts of VIRT, refer to the following documentation:

**Front End**

[Front End Setup and Development](docs/front-end-setup.md)

[General Information](docs/front-end-general.md)

[Session and Login pages](docs/front-end-session-page.md)

[Filter Page](docs/front-end-filter-page.md)

[Video Page](docs/front-end-video-page.md)

**Back End**

[Back End Setup](docs/back-end-setup.md)
