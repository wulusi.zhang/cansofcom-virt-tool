/* Code for front-end console */
'use strict'

var consoleOutput = "";
var lineNum = 0;

// we just grab everything that the backend sends to the front end
$(document).ready(function() {
    $('#debug-output').text(
        consoleOutput = "VIRT Frontend Console\nUsing UIBuilder Front-End Version: " + uibuilder.get('version')
    );

    uibuilder.debug(true)

    uibuilder.onChange('msgsReceived', function(newVal){
        UpdateConsoleText('received message: ' + JSON.stringify(uibuilder.get('msg')))
    })

    uibuilder.onChange('msgsCtrl', function (newVal) {
        UpdateConsoleText('received ctrl message: ' + JSON.stringify(uibuilder.get('ctrlMsg')))
    })

    uibuilder.onChange('msgsSent', function (newVal) {
        UpdateConsoleText('sent message: ' + JSON.stringify(uibuilder.get('sentMsg')))
    })

    uibuilder.onChange('ioConnected', function (newVal) {
        UpdateConsoleText('Socket.IO Connection Status Changed: ' + newVal)
    })
})

function UpdateConsoleText(stringInput) {
    lineNum++;

    consoleOutput += "\n" + lineNum + ": " + stringInput;

    $('#debug-output').text(consoleOutput)

    //console.info(stringInput);
}