$(document).ready(function () {
    // pull up the loading popup
    $('#loading-modal').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });

    // when we're ready to get the data from the backend
    uibuilder.onChange('msgsCtrl', function (newVal) {
        if (uibuilder.get('ctrlMsg').uibuilderCtrl == "ready for content") {
            $('#loading-modal').modal('hide');
        }
    });

    // we wait for a message from the backend
    uibuilder.onChange('msg', function (newVal) {
        // when we're done loading
        if (uibuilder.get('msg').topic == "LOAD DONE") {
            $('#loading-modal').modal('hide');
            window.location.href = "filter.html";
        }
    });
});

// when the user loads up a database
function loadDatabaseLoc() {
    // pull up the loading popup
    $('#loading-modal').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });

    uibuilder.send("LOAD DATA");
}
