var myVideo = document.getElementById("video");
var wasPaused = false;

// called when the pause/play button is pressed
function playPause() {
    // pause/play the video and change the button icon
    if (myVideo.paused) {
        myVideo.play();
        $('#transport-pause').css('display', 'inherit');
        $('#transport-play').css('display', 'none');
    }
    else {
        myVideo.pause();
        $('#transport-pause').css('display', 'none');
        $('#transport-play').css('display', 'inherit');
    }
}

// update the slider which shows the video progress
function updateSliderPos () {
    var time = myVideo.currentTime;
    var maxTime = myVideo.duration;
    var maxVal = $('#vid-slider').prop('max');

    $('#vid-slider').val((time / maxTime) * maxVal);
}

// when the user scrubs through on the slider
$(document).on('input', '#vid-slider', function () {
    wasPaused = myVideo.paused;
    myVideo.pause();

    var target = $('#vid-slider').val() / $('#vid-slider').prop('max');
    var maxTime = myVideo.duration;

    myVideo.currentTime = target * maxTime;
});

// should play the video if it was playing before
$(document).on('change', '#vid-slider', function () {
    if (!wasPaused) {
        myVideo.play();
        $('#transport-pause').css('display', 'inherit');
        $('#transport-play').css('display', 'none');
    }
});

myVideo.addEventListener('timeupdate', updateSliderPos);
