// Naming Key for objects and events
var IDNaming = 
{
    ObjectNames: [
        "Object 1",
        "Object 2",
        "Object 3",
        "Object 4",
        "Object 5",
    ],
    EventNames: [
        "Event 01",
        "Event 02",
        "Event 03",
        "Event 04",
        "Event 05",
        "Event 06",
        "Event 07",
        "Event 08",
        "Event 09",
        "Event 10",
        "Event 11",
        "Event 12",
    ]
};

// main variable to hold raw data
var VideoEditData;

var graphWidth;
var graphHeight;
var width;
var height;
var dynamicGraphSizeValues = {width: 200, height: 30};
var margin = {
    top: 20,
    bottom: 0,
    left: 100,
    right: 0
};

var timelineStartDate;
var timelineEndDate;
var xScale;
var yScaleMain;
var dateParser = d3.timeParse("%Y,%m,%d,%H,%M,%S");
var timeParser = d3.timeParse("%H,%M,%S");

// data thats been sorted and made sense of
var cleanObjTimeData = [];
var cleanObjData = [];
var cleanEventData = [];
var cleanObjBBoxData = [];
var cleanEventBBoxData = [];

var svg; // timeline svg
var lineMarker; // line showing where the video is on the timeline

var displayVideo; // video that is being played
var videoFramerate = 30; // assuming the framerate is 30, if its 29.97 things get weird
var displayVideoWidth = 0;
var displayVideoHeight = 0;
var displayVideoXScale;
var displayVideoYScale;
var videoSVG; // the "graph" thats being used to draw the BBox

// for loading the files
var objFile;
var evntFile;
var isLoaded = [false, false];

$(document).ready(function() {
    // pull up the loading popup
    $('#loading-modal').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });

    uibuilder.onChange('msgsCtrl', function(newVal) {

        // when we're ready to get the data
        if (uibuilder.get('ctrlMsg').uibuilderCtrl == "ready for content") {

            // even though technically we don't need to here, read the data
            var filename = location.toString().split("#")[1];

            $.getJSON('/' + filename + '.events.txt', function (data) {
                // set the video
                $('#video').attr('src', data.videoPath.replace('ParsedData', ''));
                // finally grab the ref to the video
                displayVideo = document.getElementById("video");

                evntFile = data;
                checkIfReady("event");
            });

            $.getJSON('/' + filename + '.objects.txt', function (data) {
                objFile = data;
                checkIfReady("obj");
            });

        }

    });
});

// check if we loaded all the files needed to display the data
function checkIfReady(loadID) {
    if (loadID == "event") { isLoaded[0] = true; }
    if (loadID == "obj") { isLoaded[1] = true; }

    // if everything is loaded, display the timeline
    if (isLoaded[0] && isLoaded[1]) {

        // make sure the variables are empty
        cleanObjTimeData = [];
        cleanObjData = [];
        cleanEventData = [];
        cleanObjBBoxData = [];
        cleanEventBBoxData = [];

        VideoEditData = [evntFile, objFile];
        displayVideo.addEventListener("loadedmetadata", setupGraph);

        // allows the marker to be updated when playing
        displayVideo.addEventListener("timeupdate", updateMarkerPos);

        // main drawing loop for the bbox
        displayVideo.addEventListener("timeupdate", drawBoundingBoxes);
        // makes the marker jump when the video is scrubbed
        displayVideo.addEventListener("seeking", moveMarkerPos);
        // when the video is ready to be played
        displayVideo.addEventListener("loadedmetadata", setupBoundingBox);

        // once we're done loading, put this away
        $('#loading-modal').modal('hide');
    }
}

// drawing the entire timeline here
function setupGraph() {
    var totalFrames = displayVideo.duration * videoFramerate;
    var dateNow = dateParser("2019,05,07,15,22,08");
    var dateEnd = d3.timeSecond.offset(dateNow, displayVideo.duration);

    timelineStartDate = dateNow;
    timelineEndDate = dateEnd;

    // have the timeline be as long as it needs to based on the video length
    graphWidth = d3.timeMinute.count(timelineStartDate, timelineEndDate) * dynamicGraphSizeValues.width;

    // setup the clean data based on max ammount of obj/events possible

    for (var i = 0; i < IDNaming.ObjectNames.length; i++) {
        cleanObjTimeData.push([]);
    }

    for (var i = 0; i < IDNaming.EventNames.length; i++) {
        cleanEventData.push([]);
    }

    for (var i = 0; i < totalFrames; i++) {
        cleanObjBBoxData.push([]);
        cleanEventBBoxData.push([]);
    }

    width = graphWidth - (margin.left + margin.right);
    height = graphHeight - (margin.top + margin.bottom);

    // we finally parse the data here
    parseBackendData(VideoEditData);

    xScale = d3.scaleTime()
        .domain([timelineStartDate, timelineEndDate])
        .range([0, width])
    ;

    // setup to count for the xscale
    var scaleLimit = [];
    var numOfSections = 0; // count how many sections we need
    numOfSections += cleanEventData.length + 1; // the events plus its header

    // one for every object and header
    for (var i = 0; i < cleanObjTimeData.length; i++) {
        numOfSections += cleanObjTimeData[i].length + 1;
    }

    // push to a temp array
    for (var i = 0; i < numOfSections; i++) {
        scaleLimit.push(i);
    }

    // calc the height here
    graphHeight = numOfSections * dynamicGraphSizeValues.height;
    height = graphHeight - (margin.top + margin.bottom);

    // finally the yscale can be made
    yScaleMain = d3.scaleBand()
        .domain(scaleLimit)
        .range([0, height])
        .padding(0.2)
    ;

    // create svg graphic
    svg = d3.select('#timeline')
        .append('svg')
        .attr('width', graphWidth)
        .attr('height', graphHeight)
        .append('g')
        .attr('transform', "translate(" + margin.left + "," + margin.top + ")")
    ;

    // background
    svg.append('g')
        .append('rect')
        .attr('width', width)
        .attr('height', height)
        .attr('class', 'timeline-bg')
        .on('click', function () {
            moveToTimeOnVideo(xScale.invert(d3.mouse(this)[0]));
        })
    ;

    // x axis with labels
    svg.append('g')
        .attr('class', 'vis-text axis xAxis')
        .attr('transform', 'translate(' + 0 + ',' + 0 + ')')
        .call(d3.axisTop(xScale)
            .ticks(d3.timeMinute, 1)
            // .tickFormat(d3.timeFormat("%b %d %H:%M"))
            .tickFormat(d3.timeFormat("%H:%M"))
        )
    ;

    // x axis without
    svg.append('g')
        .attr('color', 'grid')
        .attr('transform', 'translate(' + 0 + ',' + 0 + ')')
        .call(d3.axisTop(xScale)
            .ticks(d3.timeSecond, 5)
            .tickFormat("")
        )
    ;

    // x axis grid lines
    svg.append('g').selectAll('line.xgrid')
        .data(xScale.ticks(d3.timeSecond.every(30)))
        .enter().append('line')
        .attr('class', 'grid xgrid')
        .attr('x1', function (d) { return xScale(d); })
        .attr('x2', function (d) { return xScale(d); })
        .attr('y1', 0)
        .attr('y2', height)
    ;

    // y axis grid lines
    svg.append('g').selectAll('line.ygrid')
        .data(yScaleMain.domain())
        .enter().append('line')
        .attr('class', 'grid ygrid')
        .attr('stroke-opacity', 0.4)
        .attr('x1', 0)
        .attr('x2', width)
        .attr('y1', function (d, i) {
            return yScaleMain(i) + yScaleMain.bandwidth() / 2;
        })
        .attr('y2', function (d, i) {
            return yScaleMain(i) + yScaleMain.bandwidth() / 2;
        })
    ;

    // draw the y axis after the object headers

    // event header
    svg.append('g').attr('class', 'object-header')
        .append('rect')
        .attr('class', 'object-header')
        .attr('x', 0 - margin.left) // we want to draw the headers through to the axis labels
        .attr('y', yScaleMain(0))
        .attr('width', width + margin.left) // add the extra space in
        .attr('height', yScaleMain.bandwidth())
    ;

    // draw events
    for (var index = 0; index < cleanEventData.length; index++) {

        svg.append('g').attr('class', 'events')
            .selectAll('g.events')
            .data(cleanEventData[index])
            .enter().append('rect')
            .attr('class', 'eventRect event' + (index + 1))
            .attr('x', function (d) {
                return xScale(d.start);
            })
            .attr('y', function (d, i) {
                return yScaleMain(index + 1);
            })
            .attr('width', function (d) {
                return xScale(d.end) - xScale(d.start);
            })
            .attr('height', function (d) {
                return yScaleMain.bandwidth();
            })
            // when the event box is clicked, move to the timestamp
            .on('click', function (d) {
                moveToTimeOnVideo(d.start);
            })
        ;
    }

    // y scale for the ingrain linegraph
    var headerYScales = [];
    // the main y scale location for the headers
    var objHeaderLocation = [];
    // add 1 space for event header and one for the first obj header and events
    var displaySpacing = cleanEventData.length + 2;

    // draw object headers locations
    for (var index = 0; index < cleanObjTimeData.length; index++) {
        // calc the correct location to display on the yscale 
        if (index > 0) {
            displaySpacing += cleanObjTimeData[index - 1].length + 1;
        }

        // header
        svg.append('g').attr('class', 'object-header')
            .append('rect')
            .attr('class', 'object-header')
            .attr('x', 0 - margin.left) // we want to draw the headers through to the axis labels
            .attr('y', yScaleMain(displaySpacing - 1))
            .attr('width', width + margin.left) // add the extra space in
            .attr('height', yScaleMain.bandwidth())
        ;

        // keep track of where this header is
        objHeaderLocation.push(displaySpacing - 1);

        // objects
        svg.append('g').attr('class', 'objects')
            .selectAll('g.objects')
            .data(cleanObjTimeData[index])
            .enter().append('rect')
            .attr('class', 'eventRect obj' + (index + 1))
            .attr('x', function (d) {
                return xScale(d.start);
            })
            .attr('y', function (d, i) {
                return yScaleMain(i + displaySpacing);
            })
            .attr('width', function (d) {
                return xScale(d.end) - xScale(d.start);
            })
            .attr('height', function (d) {
                return yScaleMain.bandwidth();
            })
            .on('click', function (d) {
                moveToTimeOnVideo(d.start);
            })
        ;

        // make the y scale for obj line graph
        headerYScales[index] = d3.scaleLinear()
            .domain([0, cleanObjTimeData[index].length + 1])
            .range([yScaleMain(displaySpacing - 1) + yScaleMain.bandwidth(), yScaleMain(displaySpacing - 1)])
        ;
    }

    // y axis w/ labels
    svg.append('g')
        .attr('class', 'vis-text axis yAxis')
        .attr('transform', 'translate (' + 0 + ',' + 0 + ')')
        .call(d3.axisLeft(yScaleMain)
            .tickFormat(function (d, i) {
                var returnLabel = "";

                // if we're at the top, thats the events
                if (i == 0) {
                    returnLabel = "Events";
                } else if (i < objHeaderLocation[0]) {
                    returnLabel = IDNaming.EventNames[
                        cleanEventData[i-1][0].type
                    ];
                }

                for (var index = 0; index < objHeaderLocation.length; index++) {
                    if (i == objHeaderLocation[index]) {
                        returnLabel = IDNaming.ObjectNames[index];
                        break;
                    }
                    else if (i > objHeaderLocation[index]) {
                        returnLabel = IDNaming.ObjectNames[index] + " #" + (i - objHeaderLocation[index]);
                    }
                }

                return returnLabel;
            })
        )
    ;

    // draw object line graph
    svg.selectAll('path.objectDisplay')
        .data(cleanObjData)
        .enter().append('path')
        .attr('class', function (d, i) {
            return 'object-display obj' + (i + 1);
        })
        .attr('d', d3.line()
            .x(function (d) { return xScale(d.timestamp); })
            .y(function (d) { return headerYScales[d.type](d.value); })
            .curve(d3.curveStepAfter)
        )
    ;

    // draw line marker
    lineMarker = svg.append('line')
        .attr('class', 'marker')
        .attr('stroke', 'red')
        .attr('stroke-width', 2)
        .attr('x1', 0)
        .attr('x2', 0)
        .attr('y1', 0 - margin.top)
        .attr('y2', height - margin.top)
    ;
}

// turn the raw data into useful variables
function parseBackendData (d) {
    // temp containers to seperate the data
    var objs;
    var evts;

    for (var i = 0; i < d.length; i++) {
        if (d[i].fileType == "events") {
            evts = Object.values(d[i].cookedData);
        }
        else if (d[i].fileType == "objects") {
            objs = Object.values(d[i].cookedData);
        }
    }

    // find object bbox data
    for (var index = 0; index < objs.length; index++) {
        for (var i = 0; i < objs[index].bboxData.length; i++) { 
            cleanObjBBoxData[Number(objs[index].startFrame) + i].push({
                objectType: Number(objs[index].objectType),
                x: Number(objs[index].bboxData[i][0]),
                y: Number(objs[index].bboxData[i][1]),
                w: Number(objs[index].bboxData[i][2]),
                h: Number(objs[index].bboxData[i][3])
            });
        }
    }

    // find event bbox data
    for (var index = 0; index < evts.length; index++) {
        for (var i = 0; i < evts[index].bboxData.length; i++) {
            cleanEventBBoxData[Number(evts[index].startFrame) + i].push({
                eventType: Number(evts[index].eventType),
                x: Number(evts[index].bboxData[i][0]),
                y: Number(evts[index].bboxData[i][1]),
                w: Number(evts[index].bboxData[i][2]),
                h: Number(evts[index].bboxData[i][3])
            });
        }
    }

    // find where object starts and ends
    for (var i = 0; i < objs.length; i++) {
        cleanObjTimeData[Number(objs[i].objectType)].push({
            start: convertFrameToTime(objs[i].startFrame),
            end: convertFrameToTime(objs[i].endFrame),
            type: Number(objs[i].objectType)
        });
    }

    // find where events start and ends
    for (var i = 0; i < evts.length; i++) {
        cleanEventData[Number(evts[i].eventType)].push({
            start: convertFrameToTime(evts[i].startFrame),
            end: convertFrameToTime(evts[i].endFrame),
            type: Number(evts[i].eventType)
        });
    }

    // clean up the array
    cleanEventData = cleanEventData.filter(function(el) {
        if (el.length > 0)
            return el;
    });

    for (var i = 0; i < cleanObjTimeData.length; i++) {
        cleanObjData.push([]);
    }

    // put in start and end timestamps
    for (var i = 0; i < cleanObjData.length; i++) {
        cleanObjData[i].push({
            timestamp: timelineStartDate,
            value: 0,
            type: i
        },
        {
            timestamp: timelineEndDate,
            value: 0,
            type: i
        });
    }

    // start counting when objects are seen and unseen
    for (var i = 0; i < objs.length; i++) {
        cleanObjData[objs[i].objectType].push({
            timestamp: convertFrameToTime(objs[i].startFrame),
            value: 1,
            type: Number(objs[i].objectType) // addition
        },
        {
            timestamp: convertFrameToTime(objs[i].endFrame),
            value: -1,
            type: Number(objs[i].objectType) // addition
        });
    }

    // sort and count
    for (var i = 0; i < cleanObjData.length; i++) {
        // sort by date
        cleanObjData[i].sort(function (a, b) {
            return new Date(a.timestamp) - new Date(b.timestamp);
        })

        // start counting how many objects are at a timestamp
        var totalCounter = 0;
        for (var j = 0; j < cleanObjData[i].length; j++) {
            totalCounter = totalCounter + cleanObjData[i][j].value;
            cleanObjData[i][j].value = totalCounter;
        }
    }

    // // clean up the object arrays
    // cleanObjTimeData = cleanObjTimeData.filter(function (el) {
    //     if (el.length > 0)
    //         return el;
    // });

    // cleanObjData = cleanObjData.filter(function (el) {
    //     if (el.length > 2)
    //         return el;
    // });
}

// when we give a frame, turn it into a timestamp
function convertFrameToTime (frames) {
    var frameToSec = Math.round(Number(frames) / videoFramerate);

    return d3.timeSecond.offset(timelineStartDate, frameToSec);
}

/// Binding to Video and Box Display Area ///
function updateMarkerPos () {
    var time = displayVideo.currentTime;
    var maxTime = displayVideo.duration;
    var videoLocation = width * (time / maxTime);

    lineMarker
        .transition(100).ease(d3.easeLinear, 1)
        .attr('x1', videoLocation)
        .attr('x2', videoLocation)
        .attr('y1', 0 - margin.top)
        .attr('y2', height)
    ;
}

// move the marker with the video progress
function moveMarkerPos () {
    var time = displayVideo.currentTime;
    var maxTime = displayVideo.duration;
    var videoLocation = width * (time / maxTime);

    lineMarker
        .attr('x1', videoLocation)
        .attr('x2', videoLocation)
        .attr('y1', 0 - margin.top)
        .attr('y2', height)
    ;
}

// jump to a point in the video
function moveToTimeOnVideo (targetTime) {
    // using the xscale to find the distance between the two, accuracy is irrelevant
    var percent = xScale(targetTime) / xScale(timelineEndDate); 
    var maxTime = displayVideo.duration;

    displayVideo.pause();
    displayVideo.currentTime = maxTime * percent;
}

// setup the graphic for BBox data
function setupBoundingBox () {
    displayVideoWidth = d3.select('#video').style("width");
    displayVideoHeight = d3.select('#video').style("height");

    displayVideoXScale = d3.scaleLinear()
        .domain([0, displayVideo.videoWidth])
        .range([0, displayVideoWidth])
    ;

    displayVideoYScale = d3.scaleLinear()
        .domain([0, displayVideo.videoHeight])
        .range([0, displayVideoHeight])
    ;

    videoSVG = d3.select('#video-vis')
        .append('svg')
        .attr('width', displayVideoWidth)
        .attr('height', displayVideoHeight)
    ;

    // bg rect
    videoSVG.append('rect')
        .attr('x', 0)
        .attr('y', 0)
        .attr('width', displayVideoWidth)
        .attr('height', displayVideoHeight)
        .attr('fill', 'transparent')
    ;
}

// logic to draw the BBoxes
function drawBoundingBoxes () {
    // we get an error at the very end of the video if we use 30 since technically we're missing a couple of frames at 29.97 fps
    var currFrame = Math.round(displayVideo.currentTime * videoFramerate);

    /* 
        Look through the clean BBox data, if there is information to display a bounding box, then do so
    */
    if (cleanObjBBoxData[currFrame].length > 0) {
        // clear out everything first
        videoSVG.selectAll('.bbox').remove();

        // object bounding boxes
        videoSVG.selectAll('.bbox.bbox-box.obj')
            .data(cleanObjBBoxData[currFrame])
            .enter().append('rect')
            .attr('class', function (d) {
                return 'bbox bbox-box obj obj' + (d.objectType + 1);
            })
            .attr('x', function (d) {
                return displayVideoXScale(d.x);
            })
            .attr('y', function (d) {
                return displayVideoYScale(d.y);
            })
            .attr('width', function (d) {
                return displayVideoXScale(d.w);
            })
            .attr('height', function (d) {
                return displayVideoYScale(d.h);
            })
        ;

        // events bounding boxes
        videoSVG.selectAll('.bbox.bbox-box.event')
            .data(cleanEventBBoxData[currFrame])
            .enter().append('rect')
            .attr('class', function (d) {
                return 'bbox bbox-box event event' + (d.eventType + 1);
            })
            .attr('x', function (d) {
                return displayVideoXScale(d.x);
            })
            .attr('y', function (d) {
                return displayVideoYScale(d.y);
            })
            .attr('width', function (d) {
                return displayVideoXScale(d.w);
            })
            .attr('height', function (d) {
                return displayVideoYScale(d.h);
            })
        ;

        // // background behind label text
        // videoSVG.selectAll('.bbox.bbox-label')
        //     .data(cleanObjBBoxData[currFrame])
        //     .enter().append('rect')
        //     .attr('class', function (d) { 
        //         return 'bbox bbox-label obj' + (d.objectType + 1); 
        //     })
        //     .attr('x', function (d) { return displayVideoXScale(d.x); })
        //     .attr('y', function (d) { return displayVideoYScale(d.y); })
        //     .attr('width', function (d) { return displayVideoXScale(d.w); })
        //     .attr('height', 15)
        // ;

        // // label text
        // videoSVG.selectAll('.bbox.bbox-text')
        //     .data(cleanObjBBoxData[currFrame])
        //     .enter().append('text')
        //     .attr('class', 'bbox bbox-text')
        //     .attr('alignment-baseline', 'hanging')
        //     .text(function (d, i) { 
        //         return "" + (d.objectType + 1); 
        //     })
        //     .attr('x', function (d) { return displayVideoXScale(d.x); })
        //     .attr('y', function (d) { return displayVideoYScale(d.y); })
        // ;
    }
}
