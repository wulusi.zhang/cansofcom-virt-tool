/* Main javascript for front end filtering */

var databaseFiles = [];
var randDatestampStart = d3.timeParse("%Y-%m-%d")("2019-08-11")
var randHourRange = 36;

$(document).ready(function () {
    // pull up the loading popup
    $('#loading-modal').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });

    $('#no-results').hide();
    $('#no-date').hide();

    // when we're ready to get the data
    uibuilder.onChange('msgsCtrl', function (newVal) {

        if (uibuilder.get('ctrlMsg').uibuilderCtrl == "ready for content") {

            $.getJSON('/FilterData.txt', function (data) {
                
                for (var i = 0; i < data.length; i++) {
                    if (data[i].fileType == "events") {
                        storeIntoDatabase(data[i], true);
                    }
                    else if (data[i].fileType == "objects") {
                        storeIntoDatabase(data[i], false);
                    }
                }

            });

            // once we're done loading, put this away
            $('#loading-modal').modal('hide');
        }

    });

});

// grab the data from the frontend and store it
function storeIntoDatabase (d, isEvent) {
    var dupeCheck = -1;

    var name = d.videoPath.split("\\");
    name = name[name.length - 2];

    // check if this entry exists
    for (var i = 0; i < databaseFiles.length; i++) {
        if (d.videoPath === databaseFiles[i].videoPath) {
            dupeCheck = i;
            break;
        }
    }

    // turn the cooked data object into an array
    var cookedDataArr = Object.values(d.cookedData);

    // if does exist, add the new info it otherwise add a new entry
    if (dupeCheck > -1) {
        if (isEvent) {
            databaseFiles[dupeCheck].events = cookedDataArr;
            databaseFiles[dupeCheck].eventTypes = countEvents(cookedDataArr);
        }
        else {
            databaseFiles[dupeCheck].objects = d.cookedData;
            databaseFiles[dupeCheck].objectTypes = countObjects(cookedDataArr);
        }
    }
    else {
        if (isEvent) {
            databaseFiles.push({
                name: name,
                timestamp: makeRandomTimeStamp(randDatestampStart, randHourRange),
                videoPath: d.videoPath,
                objects: [],
                objectTypes: [],
                events: cookedDataArr,
                eventTypes: countEvents(cookedDataArr)
            });
        }
        else {
            databaseFiles.push({
                name: name,
                timestamp: makeRandomTimeStamp(randDatestampStart, randHourRange),
                videoPath: d.videoPath,
                objects: cookedDataArr,
                objectTypes: countObjects(cookedDataArr),
                events: [],
                eventTypes: []
            });
        }
    }
}

// counts how many different objects there are and returns an array
function countObjects (d) {
    var returnArr = [];

    for (var i = 0; i < d.length; i++) {
        var val = Number(d[i].objectType);

        if (returnArr.includes(val) === false) {
            returnArr.push(val);
        }
    }

    return returnArr;
}

// counts how many different events there are and returns an array
function countEvents (d) {
    var returnArr = [];

    for (var i = 0; i < d.length; i++) {
        var val = Number(d[i].eventType);

        if (returnArr.includes(val) === false) {
            returnArr.push(val);
        }
    }

    return returnArr;
}

// displays the filter results and use the JQuery template plugin
function displayFilterResults (data) {
    var templatedResults = [];

    for (var i = 0; i < data.length; i++) {
        templatedResults.push({
            idName: data[i].name,
            idLoc: data[i].videoPath
        });
    }
    
    $('#results-list').loadTemplate("templates/result.html", templatedResults);
}

// displays data for the visualiser/"heatmap"
function sendToVis (data, info) {
    var visGraphData = {
        events: [],
        objects: [],
        videos: [],
        searchInfo: 0
    };

    visGraphData.searchInfo = info;

    for (var i = 0; i < data.length; i++) {
        // make sure the data is objects
        var evtsData = Object.values(data[i].events);
        var objsData = Object.values(data[i].objects);

        for (var j = 0; j < evtsData.length; j++) {
            visGraphData.events.push({
                eventType: Number(evtsData[j].eventType),
                eventStartTimestamp: frameToTimestamp(data[i].timestamp, evtsData[j].startFrame),
                eventEndTimestamp: frameToTimestamp(data[i].timestamp, evtsData[j].endFrame)
            });
        }

        for (var k = 0; k < objsData.length; k++) {
            visGraphData.objects.push({
                objectType: Number(objsData[k].objectType),
                objectStartTime: frameToTimestamp(data[i].timestamp, objsData[k].startFrame),
                objectEndTime: frameToTimestamp(data[i].timestamp, objsData[k].endFrame)
            });
        }

        // // video information
        // visGraphData.videos.push({
        //     name: data[i].name,
        //     videoStartTime: 0,
        //     videoEndTime: 0
        // });
    }

    console.log(visGraphData);
    DisplayVisualiser(visGraphData);
}

// When the user pressed the "Filter" button
function sendFilterQuery () {
    var qStartDate = d3.timeParse("%Y-%m-%dT%H:%M")($('#date-start').val());
    var qEndDate = d3.timeParse("%Y-%m-%dT%H:%M")($('#date-end').val());
    var includeEvents = [];
    var includeObjects = [];

    var filterResults = [];

    $('#no-results').hide();
    $('#no-date').hide();
    $('.filter-results').remove();

    if (qStartDate == "" || qEndDate == "") { 
        $('#no-date').show();
        return; 
    }

    // IDNaming is from vis-graph.js
    // if the checkbox is checked, push in into an array to check
    for (var i = 0; i < IDNaming.EventNames.length; i++) {
        if ($('#eventID0' + (i + 1)).prop('checked')) {
            includeEvents.push(i);
        }
    }

    for (var i = 0; i < IDNaming.ObjectNames.length; i++) {
        if ($('#objectID0' + (i + 1)).prop('checked')) {
            includeObjects.push(i);
        }
    }

    // loop through the database
    for (var i = 0; i < databaseFiles.length; i++) {
        // if the video is within the quered timestamp
        if (databaseFiles[i].timestamp >= qStartDate 
            && databaseFiles[i].timestamp <= qEndDate) {
            
            // if the video contains the objects we want to look for OR events we want to look for
            if (databaseFiles[i].eventTypes.some(r => includeEvents.indexOf(r) >= 0)
                && databaseFiles[i].objectTypes.some(r => includeObjects.indexOf(r) >= 0)) {
                filterResults.push(databaseFiles[i]);
            }
        }
    }

    //console.log(filterResults);

    // if we found nothing through the filter
    if (filterResults.length < 1) {
        $('#no-results').show();

        // from vis.graph.js - remove the current visualisation if theres nothing
        resetGraph();
    } 
    else {
        // display the results as cards
        displayFilterResults(filterResults);

        // vis-graph.js logic to display the vis
        sendToVis(filterResults, {
            startDate: qStartDate,
            endDate: qEndDate,
            searchedEvents: includeEvents,
            searchedObjects: includeObjects
        });

        // logic below sends info to the backend to display on the map

        var names = [];

        for (var i = 0; i < filterResults.length; i++ ) {
            names.push(filterResults[i].name);
        }

        uibuilder.send({
            topic: "MAP DATA",
            payload: names
        });
    }
}

function makeRandomTimeStamp (inTimeStamp, hourRange) {
    var rand = Math.random() * hourRange;
    var returnTime = d3.timeHour.offset(inTimeStamp, rand);

    // this is an easy way to know where all the videos are
    console.log(returnTime);

    return returnTime;
}

// converts frame data to time
function frameToTimestamp (sTimeStamp, frameStart) {
    return d3.timeSecond.offset(sTimeStamp, (frameStart / 30));
}

// when the user clicks on a result, naviagtes to the video editor
function navToVideo (vidID) {
    window.location.href = "video.html#" + vidID; 
}
