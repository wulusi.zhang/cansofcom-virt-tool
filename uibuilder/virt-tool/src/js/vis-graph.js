var emptyData =
{
    events: [
        {
            eventType: 0,
            eventStartTimestamp: "2019,05,07,00,00,00",
            eventEndTimestamp: "2019,05,07,00,00,00"
        }
    ],
    videos: [
        {
            name: "",
            videoStartTime: "2019,05,07,00,00,00",
            videoEndTime: "2019,05,07,00,00,00"
        }
    ],
    objects: [
        {
            objectType: 0,
            objectStartTime: "2019,05,07,00,00,00",
            objectEndTime: "2019,05,07,00,00,00"
        }
    ],
    searchInfo: {
        searchedObjects: [0],
        searchedEvents: [0]
    }
};

// Naming Key for objects and events
var IDNaming =
{
    ObjectNames: [
        "Object 1",
        "Object 2",
        "Object 3",
        "Object 4",
        "Object 5",
    ],
    EventNames: [
        "Event 01",
        "Event 02",
        "Event 03",
        "Event 04",
        "Event 05",
        "Event 06",
        "Event 07",
        "Event 08",
        "Event 09",
        "Event 10",
        "Event 11",
        "Event 12",
    ]
};

var GraphData;

var cleanObjData = [];
var cleanEventTimeData = [];
var cleanVideoTimeData = [];
var filterStartDate;
var filterEndDate;
var dateParser = d3.timeParse("%Y,%m,%d,%H,%M,%S");

// these are relative offset values you can modify
var marginOffset = {
    top: 0.1,
    bottom: 0.1,
    left: 0.2, // extra space for the y axis labels
    right: 0.1
};

var botToTopRatio = 0.6;

var margin;
var pageWidth;
var pageHeight;
var width;
var height;
var topHeight;
var bottomHeight;
var yScaleFull;
var yScaleTop;
var yScaleBottom;
var xScaleTime;
var maxNumOfObjects = 1;

var svg;
var objectLegend;
var tooltip;

// called to start drisplaying information
function DisplayVisualiser(incomingData) {
    resetGraph();

    GraphData = incomingData;

    setupGraphValues();
    resize(true); // call the resize to setup the values for the first time
    parseBackEndData(GraphData);
    setupGraph();
}

// called to remove the graph
function resetGraph () {
    $('#graph svg').remove();

    // clear out anything thats been there before
    if (objectLegend != null) {
        objectLegend.remove();
    }

    cleanObjData = [];
    cleanEventTimeData = [];
    cleanVideoTimeData = [];
}

// setup values to stard displaying the grab
function setupGraphValues() {
    filterStartDate = GraphData.searchInfo.startDate;
    filterEndDate = GraphData.searchInfo.endDate;

    for (var i = 0; i < IDNaming.ObjectNames.length; i++) {
        cleanObjData.push([]);
    }
}

// turn the raw data into useable data 
function parseBackEndData(d) {
    // put in start and end timestamps
    for (var i = 0; i < cleanObjData.length; i++) {
        cleanObjData[i].push({
            timestamp: filterStartDate,
            value: 0
        },
        {
            timestamp: filterEndDate,
            value: 0
        });
    }

    // start counting when objects are seen and unseen
    for (var i = 0; i < d.objects.length; i++) {
        cleanObjData[d.objects[i].objectType].push({
            timestamp: d.objects[i].objectStartTime,
            value: 1
        },
        {
            timestamp: d.objects[i].objectEndTime,
            value: -1
        });
    }

    // sort and count
    for (var i = 0; i < cleanObjData.length; i++) {
        // sort by date
        cleanObjData[i].sort(function (a, b) {
            return new Date(a.timestamp) - new Date(b.timestamp);
        })

        // start counting how many objects are at a timestamp
        var totalCounter = 0;
        for (var j = 0; j < cleanObjData[i].length; j++) {
            totalCounter = totalCounter + cleanObjData[i][j].value;
            cleanObjData[i][j].value = totalCounter;

            // keep track of the most amount of objects
            if (totalCounter > maxNumOfObjects) {
                maxNumOfObjects = totalCounter;
            }
        }
    }

    // // grab when videos start and end
    // for (var i = 0; i < d.videos.length; i++) {
    //     cleanVideoTimeData.push({
    //         name: d.videos[i].name,
    //         start: d.videos[i].videoStartTime,
    //         end: d.videos[i].videoEndTime
    //     });
    // }

    // grab when events start and end
    for (var i = 0; i < d.events.length; i++) {
        cleanEventTimeData.push({
            type: d.events[i].eventType,
            start: d.events[i].eventStartTimestamp,
            end: d.events[i].eventEndTimestamp
        });
    }
}

// updates graph scales before redrawing the graph for responsive functionality
function resize(firstTime = false) {
    pageWidth = parseInt(d3.select('#graph').style("width"));
    pageHeight = parseInt(d3.select('#graph').style("height"));

    margin = {
        top: pageHeight * marginOffset.top,
        right: pageHeight * marginOffset.right,
        bottom: pageHeight * marginOffset.bottom,
        left: pageHeight * marginOffset.left
    };

    width = pageWidth - (margin.left + margin.right);
    height = pageHeight - (margin.top + margin.bottom);
    topHeight = height * botToTopRatio;
    bottomHeight = height - topHeight;

    yScaleFull = d3.scaleLinear()
        .domain([0, 5])
        .range([height, 0])
    ;

    yScaleTop = d3.scaleLinear()
        .domain([0, maxNumOfObjects])
        .range([topHeight, 0])
    ;

    yScaleBottom = d3.scaleBand()
        .domain(GraphData.searchInfo.searchedEvents)
        .range([topHeight, height])
        .padding(0.2)
    ;

    xScaleTime = d3.scaleTime()
        .domain([filterStartDate, filterEndDate])
        .range([0, width])
    ;

    var svg = d3.select('#graph').select('svg')
        .attr('width', pageWidth)
        .attr('height', pageHeight)
        .select('g')
        .attr('transform', "translate(" + margin.left + "," + margin.top + ")")
    ;

    if (!firstTime)
        updateGraph(0);
}

d3.select(window).on('resize', resize); // Call the resize function whenever a resize event occurs

// called to initally draw the graph
function setupGraph() {
    // refresh thte top scale
    yScaleTop = d3.scaleLinear()
        .domain([0, maxNumOfObjects])
        .range([topHeight, 0])
    ;

    // create svg graphic
    svg = d3.select('#graph')
        .append('svg')
        .attr('width', pageWidth)
        .attr('height', pageHeight)
        .append('g')
        .attr('transform', "translate(" + margin.left + "," + margin.top + ")")
    ;

    // bg rect
    svg.append('g')
        .append('rect')
        .attr('class', 'vis-bg')
        .attr('width', width)
        .attr('height', height)
        .attr('fill', 'transparent')
    ;

    // x axis tick lines
    svg.selectAll('line.grid')
        .data(xScaleTime.ticks(d3.timeHour.every(1)))
        .enter().append('line')
        .attr('class', function (d, i) {
            if ((i + 1) % 6 == 1) {
                return 'grid grid-heavy';
            }
            else {
                return 'grid';
            }
        })
        .attr('x1', function (d) { return xScaleTime(d); })
        .attr('x2', function (d) { return xScaleTime(d); })
        .attr('y1', 0)
        .attr('y2', height)
    ;

    // x axis
    svg.append('g')
        .attr('class', 'vis-text axis xAxis')
        .attr('transform', 'translate(' + 0 + ',' + height + ')')
        .call(d3.axisBottom(xScaleTime)
            .tickFormat(d3.timeFormat("%b %d %H:%M"))
        )
    ;

    // top y axis - the objects
    svg.append('g')
        .attr('class', 'vis-text axis yAxis yTop')
        .call(d3.axisLeft(yScaleTop)
            .ticks(5)
        )
    ;

    // bottom y axis - the events
    svg.append('g')
        .attr('class', 'vis-text axis yAxis yBot')
        .attr('color', 'white')
        .call(d3.axisLeft(yScaleBottom)
            .tickFormat(function (d, i) {
                return IDNaming.EventNames[i];
            })
        )
    ;

    // legend
    objectLegend = d3.select('#obj-legend')
        .style('padding-left', margin.left + 'px')
        .selectAll('.key-dot')
        .data(GraphData.searchInfo.searchedObjects)
        .enter().append('span')
        .text(function (d) {
            return IDNaming.ObjectNames[d];
        })
        .attr('class', function (d, i) {
            return 'key-dot obj' + (d + 1);
        })
    ;

    // tooltip
    tooltip = d3.select('.graph-tooltip')
        .style('visisbility', 'hidden')
    ;

    // draw object line graph
    svg.selectAll('path.object-display')
        .data(cleanObjData)
        .enter().append('path')
        .attr('class', function (d, i) {
            return 'object-display obj' + (i + 1);
        })
        .attr('d', d3.line()
            .x(function (d) { return xScaleTime(d.timestamp) })
            .y(function (d) { return yScaleTop(d.value) })
            .curve(d3.curveStepAfter)
        )
    ;

    // nice line seperator, drawing here to go over the line graph
    svg.append('line')
        .attr('class', 'line-seperator grid-heavy')
        .attr('color', 'white')
        .attr('x1', 0)
        .attr('x2', width)
        .attr('y1', topHeight)
        .attr('y2', topHeight)
    ;

    // // draw video highlights text
    // svg.selectAll('text.video-display')
    //     .data(cleanVideoTimeData)
    //     .enter().append('text')
    //     .attr('class', 'video-display vis-text')
    //     .text(function (d) {
    //         return d.name;
    //     })
    //     .attr('dominant-baseline', 'hanging')
    //     .attr('x', function (d) {
    //         return xScaleTime(d.start) + 5;
    //     })
    //     .attr('y', 5)
    // ;

    // // draw video highlights rects
    // svg.selectAll('rect.video-display')
    //     .data(cleanVideoTimeData)
    //     .enter().append('rect')
    //     .attr('class', 'video-display')
    //     .attr('x', function (d) {
    //         return xScaleTime(d.start);
    //     })
    //     .attr('y', 0)
    //     .attr('width', function (d) {
    //         return xScaleTime(d.end) - xScaleTime(d.start);
    //     })
    //     .attr('height', height)
    // ;

    // draw event bars
    svg.selectAll('rect.event')
        .data(cleanEventTimeData)
        .enter().append('rect')
        .attr('class', function (d) {
            return 'eventRect event' + (d.type + 1);
        })
        // quick bug fix
        .style("visibility", function (d) {
            if (d.type > GraphData.searchInfo.searchedEvents.length - 1) {
                return "hidden";
            }
            else {
                return "visible";
            }
        })
        .attr('x', function (d) {
            return xScaleTime(d.start);
        })
        .attr('y', function (d) {
            return yScaleBottom(d.type);
        })
        .attr('width', function (d) {
            return xScaleTime(d.end) - xScaleTime(d.start);
        })
        .attr('height', function (d) {
            return yScaleBottom.bandwidth();
        })
    ;

    // functionality for the tool tip to show/hide
    svg.on('mouseover', function () {
            return tooltip.style('visibility', 'visible');
        })
        // tooltip
        .on('mousemove', function () {
            var ttX = d3.mouse(this)[0] + margin.left + 20;
            var ttY = d3.mouse(this)[1] + margin.top;
            var xScaleLocation = xScaleTime.invert(d3.mouse(this)[0])

            // we basically look at whate values exist on the xScale, and display them

            // date
            $('#tt-date').text(d3.timeFormat("%b %d %H:%M")(xScaleLocation));

            // // videos
            // var videoOutput = "";

            // for (var VideoIndex = 0; VideoIndex < cleanEventTimeData.length; VideoIndex++) {
            //     if (cleanVideoTimeData[VideoIndex].start < xScaleLocation &&
            //         cleanVideoTimeData[VideoIndex].end > xScaleLocation) {
            //         videoOutput += cleanVideoTimeData[VideoIndex].name + " ";
            //     }
            // }

            // $('#tt-video').text(videoOutput);


            var objOutput = "";

            // objects
            for (var ObjTypeIndex = 0; ObjTypeIndex < cleanObjData.length; ObjTypeIndex++) {

                var ObjType = cleanObjData[ObjTypeIndex];

                for (var ObjArrayIndex = 0; ObjArrayIndex < ObjType.length; ObjArrayIndex++) {
                    if (ObjType[ObjArrayIndex].timestamp > xScaleLocation) {

                        if (ObjType[ObjArrayIndex - 1].value > 0) {
                            objOutput += IDNaming.ObjectNames[ObjTypeIndex] + " x" + ObjType[ObjArrayIndex - 1].value + ", ";
                            break;
                        }
                    }
                }
            }

            $('#tt-objs').text(objOutput);

            // events
            var eventOutput = "";

            for (var EventIndex = 0; EventIndex < cleanEventTimeData.length; EventIndex++) {
                if (cleanEventTimeData[EventIndex].start < xScaleLocation &&
                    cleanEventTimeData[EventIndex].end > xScaleLocation) {
                    eventOutput += "event " + cleanEventTimeData[EventIndex].type + ", ";
                }
            }

            $('#tt-events').text(eventOutput);

            // also remember to move the tooltip
            return tooltip
                .style("top", ttY + "px")
                .style("left", ttX + "px");
        })
        .on("mouseout", function () {
            return tooltip.style("visibility", "hidden");
        });
    ;
}

// update and animates the graph based on new data - mostly used for resizing
function updateGraph(transitionTime) {

    // we set a transition over everything
    svg.transition(transitionTime);

    svg.selectAll('rect.vis-bg')
        .attr('width', width)
        .attr('height', height)
    ;

    svg.selectAll('line.grid')
        .data(xScaleTime.ticks(d3.timeHour.every(1)))
        .attr('class', function (d, i) {
            if ((i + 1) % 6 == 1) {
                return 'grid ' + 'obj3';
            }
            else {
                return 'grid ' + 'obj';
            }
        })
        .attr('x1', function (d) { return xScaleTime(d); })
        .attr('x2', function (d) { return xScaleTime(d); })
        .attr('y1', 0)
        .attr('y2', height)
    ;

    // x axis
    svg.selectAll('.xAxis')
        .attr('transform', 'translate(' + 0 + ',' + height + ')')
        .call(d3.axisBottom(xScaleTime)
            .tickFormat(d3.timeFormat("%b %d %H:%M"))
        )
    ;

    // top y axis
    svg.selectAll('.yTop')
        .call(d3.axisLeft(yScaleTop)
            .ticks(5)
        )
    ;

    // bottom y axis
    svg.selectAll('.yBot')
        .call(d3.axisLeft(yScaleBottom)
            .tickFormat(function (d, i) {
                return IDNaming.EventNames[i];
            })
        )
    ;

    // legend 
    objectLegend.selectAll('.key-dot')
        .data(['Object 01', 'Object 02', 'Object 03'])
        .text(function (d) {
            return d;
        })
        .attr('class', function (d, i) {
            return 'key-dot obj' + (i + 1);
        })
    ;

    // draw object line graph
    svg.selectAll('path.object-display')
        .data(cleanObjData)
        .attr('d', d3.line()
            .x(function (d) { return xScaleTime(d.timestamp) })
            .y(function (d) { return yScaleTop(d.value) })
            .curve(d3.curveStepAfter)
        )
    ;

    // nice line seperator
    svg.selectAll('line.line-seperator')
        .attr('x1', 0)
        .attr('x2', width)
        .attr('y1', topHeight)
        .attr('y2', topHeight)
    ;

    // // video highlights text
    // svg.selectAll('text.video-display')
    //     .data(cleanVideoTimeData)
    //     .text(function (d) {
    //         return d.name;
    //     })
    //     .attr('dominant-baseline', 'hanging')
    //     .attr('x', function (d) {
    //         return xScaleTime(d.start) + 5;
    //     })
    //     .attr('y', 5)
    // ;

    // // video highlights rects
    // svg.selectAll('rect.video-display')
    //     .data(cleanVideoTimeData)
    //     .attr('x', function (d) {
    //         return xScaleTime(d.start);
    //     })
    //     .attr('y', 0)
    //     .attr('width', function (d) {
    //         return xScaleTime(d.end) - xScaleTime(d.start);
    //     })
    //     .attr('height', height)
    // ;

    // event bars
    svg.selectAll('rect.event')
        .data(cleanEventTimeData)
        .attr('x', function (d) {
            return xScaleTime(d.start);
        })
        .attr('y', function (d) {
            return yScaleBottom(d.type);
        })
        .attr('width', function (d) {
            return xScaleTime(d.end) - xScaleTime(d.start);
        })
        .attr('height', function (d) {
            return yScaleBottom.bandwidth();
        })
    ;
}
