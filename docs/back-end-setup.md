# Back End Setup and Configuration

## VIRT (Video Information Refinement Tool)

### Required software and settings

To run VIRT tool back end data processor the following software/packages are needed:

- [Node.js](https://nodejs.org/en/) (v10 or above)
- [Node-Red](https://nodered.org/docs/getting-started/local) (install through node.js using command `npm install -g --unsafe-perm node-red`)
- [mkdirp](https://www.npmjs.com/package/mkdirp) (install through node.js by using command `npm install -g mkdirp`)
- In settings.js uncomment line 217 and 218 add `mkdirp:require('mkdirp')` and `readline:require('readline)` to allow for mkdirp and native nodeJS readline functionality to be toggled.

![AltText](docs/res/img/backend/settings-JS.png)

### Running VIRT tool

### **Key Functional Nodes**

#### 1. Data File Reader

Responsible for reading a predetermined files of `.type` from a localized folder currently `var path = "SampleData"`  file path is a local file path contained within the current **project folder**  which is `\NodeRed Tool\SampleData` this file path can be modified to support additional subfolders and names.

Use `fileformat` parameter to change file formats being queried and passed on to **sorter** and **copier** nodes, currently `fileformat` is set to `.txt` to read VIRAT data under `.txt` format.

File reader will also identify and log out in console the number of files that did not match `fileformat`.

#### 2. Data File Sorter

This node is responsible for identifying data files names corresponding to each event/video, the node reads the first second of the file name such as `VIRAT_S_000001` of `VIRAT_S_000001.viratdata.events` and identifies all files associated with that event/video.

No customization is needed in this node.

#### 3. Data File Copier

This node is responsible for copying appropriate files from the data reader located in the predetermined folder `"SampleData"` the node will also create and make specific folder for each unique event/video with the following folder structure `copyFolder/VIRAT_S_000001/sortedData`, currently the default folder where files are copied to is set with the parameter `copyFolder = 'ParsedData\\'` this can be changed to a custom name if needed.

This node also logs information about files that are missing from the `SampleData` directory and indicates the file type that is missing in console.

`var filePathObject` is the object containing a reference to all names of files that are associated with the particular video file. It has the following format:

```javascript
var filePathObject = {
     objNum: i,
     newfileFolderName: msg.payload[i],
     videofile: msg.payload[i] + ".mp4",
     mappingFile: msg.payload[i] +  ".viratdata.mapping.txt",
     eventFile: msg.payload[i] + ".viratdata.events.txt",
     objectFile: msg.payload[i] + ".viratdata.objects.txt",
};
```

The filetype of each ```filePathObject``` would then be accessible to subsequent functions callbacks by referencing the array of which it is saved in this is ```fileNamesToCopy```. The string data that appears after each property in ```filePathObject``` can be modified to correspond to desired datafile types, as of now the section of the string that corresponds to their file types are manually coded into the value of the each property.

```javascript
     videofile: msg.payload[i] + ".mp4"
     videofile: msg.payload[i] + ".mp4",
     mappingFile: msg.payload[i] +  ".viratdata.mapping.txt",
     eventFile: msg.payload[i] + ".viratdata.events.txt",
     objectFile: msg.payload[i] + ".viratdata.objects.txt", 
```

Each of the above filetypes can be changed manually to accommodate for a new filetypes.

This node is also responsible for copying appropriate files from the data reader located in the predetermined folder `"Sampledata"` the node will also create and make specific folder for each unique event/video with the following folder structure `copyFolder/VIRAT_S_000001/sortedData`, currently the default folder where files are copied to is set with the parameter `copyFolder = 'ParsedData\\'` this can be changed to a custom name if needed.

Once the appropriate files are copied to the correct locations, this node creates a list of file paths corresponding to each existing data file that has been copied. This list is then saved into the flow variable `CopyToPathNames` and passed onto the parser for individual file processing and filtering. (Note: only copies of the original data are used strictly for parsing and reading data for output into the front end, no original input data or files will be modified).

#### 4. Data Loader & Parser

This node is responsible for reading and parsing all the copied data passed on from the previous node. The flow variable `CopyToPathNames` contains a list of all files that requires to be parsed and processed. (Note: flow variables persists between nodes and is another way in which data can be transferred between nodes other than passing it through msg.paylod)

The main for loop of this function takes the each file path within `CopyToPathNames` and parses them individually through a list of functions, the first of which identifies file paths in order to determine if they are the correct data type to be read. The data type that can be parsed is saved in the variable `readableExtensions` which is set to .txt extensions by default. The syntax is written as `/(\.type)$/i` where `.type` is the type of which the files that can be read.

The first filters determines if each file path matches the type of `readableExtensions` and logs out to console if the type does not match a readable type. 

For file types that fulfilled the readable Extensions criteria (.txt), a JavaScript object container is created to house the main body of data. `rawFile` is the default JavaScript object containing the following properties:

```javascript
 var rawFile = {
            name: fileName,
            path: filePaths[i],
            videoPath: videoFilePath,
            fileType: fileExtType,
            cookedData: {},
            //below key and property contains the data split by escape sequences
            rawData: fileData.toString().split("\r\n"),
        }
```

Where each property containing an identifier for the data that is parsed, where:

​	`fileName` is the name of the video/event that is associated with event, object and mapping 					data. (VIRAT_S_XXXXXX).

​	`path` is the exact pathway of the VIRAT data copy that is stored for reference.

​	`videoPath` is the exact pathway of the VIRAT data copy for the video file. (.mp4) 

​	`cookedData` is an empty container used to house the data that will be processed.

​	`rawData` contains split data that has been read that are ready for processing. 

The `rawData` from each file path is then fed through a series of functions to allow for data processing and parsing. `readBlockChunks`, `assignToContainer`and `writeToJson`.

`readBlockChunks` is the primary data sorter where each file is read accordingly and parsed based on their type of data. Currently events and object data are set up to be read. The parser reads each line of the split data and assigns each element of each line to unique container, some redundant data are removed to allow for efficiency and storage management. This parser/reader is set up specifically to read based on the unique data structure of each file type, taking into account the number of frames of each unique event/object ID and reading the number of lines corresponding to the duration of each event/object. The parser also keeps tally of all the lines that are read and ensures that read lines can be identified by which event and video it belongs to for further sorting in `assignToContainer` function.

For each line of data in **Event** file type, the assigned output data is store in such a way that each line of data is split up the following method:

```javascript
Object.assign(output,
                    {
                        [splitChunkData[0]]: {

                            "eventID": splitChunkData[0],
                            "eventType": splitChunkData[1],
                            "duration": splitChunkData[2],
                            "startFrame": splitChunkData[3],
                            "endFrame": splitChunkData[4],
                            "bboxData": [[
                                        splitChunkData[6],
                                        splitChunkData[7],
                                        splitChunkData[8],
                                        splitChunkData[9]
                                    		],],
                        },
                    });
```

For each line of data in **Object** file type, the assigned output data is store in such a way that each line of data is split up the following method:

```javascript
 Object.assign(output,
                    {
                        [splitChunkData[0]]: {
                            
                            "objectDuration": splitChunkData[1],
                            "objectType": splitChunkData[7],
                            "startFrame": (Number(splitChunkData[2])),
                            "endFrame": (Number(splitChunkData[2]) + 													Number(splitChunkData[1])),
                            "bboxData": [  
                                    [
                                    splitChunkData[3],
                                    splitChunkData[4],
                                    splitChunkData[5],
                                    splitChunkData[6],
                                    ],
                            ],
                        },
                    });
```

Currently only Bbox (binding box data) is saved for every frame of which events or objects and required to be displayed for the video. However this can changed to allow further data to be saved and displayed. Based on the type of data being parsed, event and object data are trimmed and stored differently in a temporary data container named `combined`. This temporary array container is used to store all the data that has been processed by `readBlockChunks` and sends the data to `assignToContainer` which stores the parsed data in the `rawFile` object that was created earlier. 

`assignToContainer` filters the data that has been parsed and funnels them to the `cookedData` container created earlier for each respective `rawFile`. Once the data is assigned, a copy of each rawFile is made without the containing Bbox data and is used as a filter dictionary for front-end UI. This file is saved under the name `filterData`. This also a quick and lighter output log of all parsed datasets from the parser.

For each set of data, **events** and **objects**, a unique file is saved for each VIRAT_S data and contains all information under the parsed format. This is controlled with the following function:

```javascript
for(var y = 0; y < rawFiles.length; y++){
    
	var nameOfFile = rawFiles[y].name;
	var fileType = rawFiles[y].fileType;
	writeToJson(rawFiles[y], `${[nameOfFile]}.${[fileType]}`);
    
}
```

Where the loop reads and creates files from all rawFile containers that were created earlier containing the parsed data. 

Below is the main that function that is used to generate and write the Json data. Note: this function is referencing the `folderName` which is set by the flow variable **copyFolder** (set in the prior node Data File Copier).  

```javascript
function writeToJson(rawFiles, name){

fs.writeFile(`${folderName}\\${name}.txt`, JSON.stringify(rawFiles), (err) => {
    
    if(err) {
    console.error(err);
    return
    }
    console.log("Log output of Json created!");
});
}
```

5. #### Data Loader & Parser

Once the above nodes have completed their functions, this node is used to send a message to front end tool to have it indicate that data processing and parsing is complete. Note: delay node of 1s is implemented before this node to prevent simultaneous read and write errors.