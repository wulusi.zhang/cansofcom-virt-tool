# Front End Pages - Login and Session Pages

The login and session pages have very little functionality, but both use Bootstrap layout and components.

## Login Page

This page is mostly for aesthetics and isn't functional as a real login page. When clicking on the "Submit" button fires an HTML onClick event to navigate to `session.html`. No real log in information is needed

The login page is also the page that the user lands on when navigating to the VIRT tool in browser. Though technically `index.html` gets loaded when navigating to the URL, the user automatically gets redirected to `login.html` and therefore can't access `index.html` at all. This was made to work around the uibuilder node limitations.

## Session Page

The information displayed on this page is hard set, none of the buttons do any actions. The menu bar at the top has only "Log Out" working. When clicking on the only database entry, it will start the data crunching process of the back end. Once the backend has finish parsing through all of the data, the page loads to `filter.html`.

While the back end is parsing through the data, a loading modal will appear. The user can't dismiss it until the front end receives a command that that back end is finished parsing the data.

## Functionality with Back End

Within `session-logic.js`, it contains the minium code needed to communicate with the backend. It simply asks the back end to parse the data, then waits until it receives a command from the back end that it is done.

![AltText](docs/res/img/frontend/sending-message.png)

In the back end there are two nodes that facilitate receiving information from the front end, a `uibuilder` node and `switch` node. The topmost `uibuilder` node output are messages sent using `uibuilder.send` function in the front end scripts, a `switch` node is used to determine what is being sent and if its a command that we were expecting then we proceed with the proceeding nodes.

![AltText](docs/res/img/frontend/get-message.png)

The other nodes to give information back to the front end is just a simple input to the `uibuilder` node. In this image its proceeded by a `function` node to simply set the payload contents to a message that the front end can understand. These messages are hard coded.

In the case of `session-logic.js`, the script is sending a `LOAD DATA` payload as a string to the backend, and expecting a `LOAD DONE` string as a payload to redirect the page to `filter.html`.

The `delay` node shown in the image above is a bug fix. Its been found that after a complex operation in the back end, sending a message to the front end immediately afterwards causes the message to not send. Delaying the message after the heavy operation ensures the message is send properly.
