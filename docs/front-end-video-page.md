# Front End Pages - Video

The video editing page is divided up into 3 main parts, and contains almost all the functionality of the original video tagging tool. The `File` and `Edit` items in the menu bar don't work, however the `Log Out` and `Back To Filter` items do.

This page relies on a special trick when its redirected from the filter page. In `filter-logic.js` it navigates to the `video.html` page with an extra bit at the end of the URL with the video the user wants to view. Then in `video-edit-logic.js`, the script looks at the url of the page to find what video it needs to load.

For example, if the url is `/virt-tool/video.html#VIRAT_S_000004` then the video the page loads is `VIRAT_S_000004`. This means that simply navigating to `video.html` will not load the page.

## Comments

The comments are placeholders using Bootstrap's according component, they don't contain any functionality.

## Timeline

The timeline is a graph that is drawn within D3.js, and its made like the visualizer in `filter.html`. The graph is drawn oversized on purpose, but the html section its drawn in is set to `overflow: scroll`, and is hooked up to the events of the video being played.

Other than the changing marker, the entire graph is static and doesn't change once its drawn.

There is no way to collapse the group of events, and the headers for objects that don't appear in the video are still drawn on timeline. These features can be added in the future.

## Video Player

The video player is a normal HTML video with a D3.js graph drawn overtop it, the D3.js graph is how the boxes are drawn over top the video all contained within `timeline-graph.js`. Since the normal video controls are blocked from use, simple transport controls are made with HTML and hooked up to control the video through the `video-edit-logic.js`.

The logic to draw the bounding boxes drawn over the video is within `timeline-graph.js`, since the script contains other functions it needs to communicate with the timeline. All of the BBox data is stored within one array with the array index representing a frame, and at every frame the script draws whatever objects are within the frame index over the video.

There is a limitation with HTML and Javascript, in that it can't check what frame a video is on. The best Chrome can do is check 10 times every second the timestamp of the video, therefore at least 50% of the BBox data isn't used. Which is also why the bounding boxes don't follow their targets smoothly, which may be a problem for fast moving targets.

There is code within `timeline-graph.js` to draw the bounding boxes with labels, however that is not used, since there isn't a happy medium between a readable text and not obscuring any other data.

There is no way to hide bounding boxes that are already drawn, though that feature can be added in the future.
