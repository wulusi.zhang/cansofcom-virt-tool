# Front End Setup and Configuration

The VIRT front end was built on top of the [uibuilder](https://github.com/TotallyInformation/node-red-contrib-uibuilder) Node-RED node, all basic configuration is done through that node itself. A limitation of the uibuilder node is the lack of preprocessor support, therefore any processing must be done manually before deployment.

All the files used by the front end are contained within `uibuilder/virt-tool/src/`.

The default files created by uibuilder, `index.html`, `index.css`, and `index.js`, are regenerated if they are moved/removed from `uibuilder/virt-tool/src/`. When navigating to the front end the default page is `index.html`, however `index.html` is modified slightly to automatically redirect to `login.html`.

If navigating to the front end displays `index.html`, then add the following code at the end of the `<head>` section of `index.html`:

```html
<script> window.location.replace("login.html"); </script>
```

## List of Tools/Plugins

The following is used specifically by the front end:

- uibuilder (Node-RED node)
- web-worldmap (Node-RED node)
- JQuery (supplied by the uibuilder node)
  - JQuery loadTemplate plugin
- Bootstrap 4.0
- SASS
- D3.js

## Modifications to settings.js

The following are made to the settings.js file for Node-RED, they are done already using the files within the repo.

At the Top:

```json
// The `https` setting requires the `fs` module. Uncomment the following
// to make it available:
var fs = require("fs");
var path = require ("path");
```

Later On:

```json
// When httpAdminRoot is used to move the UI to a different root path, the
// following property can be used to identify a directory of static content
// that should be served at http://localhost:1880/.
httpStatic: path.join(__dirname, 'ParsedData'),
```

This allows the front end to read files stored in the root folder, specifically files in the `\ParsedData` folder. Without this, the front end can only access files within `\uibuilder\virt-tool\src` which isn't easy for the backend to put files in.

### Node-RED Theme

The following is added to change the appearance of the Node-RED editor:

```json
// Customising the editor
editorTheme: {
    page: {
    title: "VIRT Node-RED Editor",
    favicon: __dirname + "/theme/logo-black.png"//, //can use '__dirname + "\\img\\favicon.png" (\\ on Windows)'
    //css: "/absolute/path/to/custom/css/file",
    //scripts: "/absolute/path/to/custom/js/file"  // As of 0.17
    },
    header: {
        title: "Video Information Refinement Tool | Node-RED Editor",
        image: __dirname + "/theme/title-white.png" //, // or null to remove image
        //url: "http://nodered.org" // optional url to make the header text/image a link to this url
    },
    projects: {
        // To enable the Projects feature, set this value to true
        enabled: false
    }
}
```

This is an extra flare given to the Node-RED editor, and can be changed. More information can be found [here](https://github.com/node-red/node-red/wiki/Design:-Editor-Themes).

## Configuring SASS for VIRT

SASS is a CSS preprocessor, and due to the limitations of uibuilder SASS is run through the command line. SASS takes the `.scss` files and converts it into one compiled `.css` file to use in our web-page, as well as many other powerful features.

Any text editor can edit `.scss` files, and SASS syntax if very similar to CSS. We recommend [installing](https://sass-lang.com/install) SASS globally and to simply run either the [update](https://sass-lang.com/documentation/cli/dart-sass#update) or [watch](https://sass-lang.com/documentation/cli/dart-sass#watch) commands.

The main SASS file is located in `uibuilder/virt-tool/src/scss/main.scss` and the processed css file is in `uibuilder/virt-tool/src/css/main.css`. Running the command below in the root folder to make SASS editing much easier:

```console
sass --watch uibuilder/virt-tool/src/scss/main.scss:uibuilder/virt-tool/src/css/main.css
```

This will watch all SASS files as you edit them, and compile to a `.css` file that is already linked in all of the front-end html. Simply save and refresh to see the changes.

## World Map Styling

The world map used in VIRT is powered by the web-worldmap node, and its basic configuration is done there. It exists at the `/worldmap` url location and within an iframe in `filter.html` with some elements removed.

Most of the styling of the pins and the maps itself is done through the web-worldmap node. To learn more, visit the [node homepage.](https://flows.nodered.org/node/node-red-contrib-web-worldmap)

## Bootstrap

Many of the HTML elements and components are styled with Bootstrap, with the main structure of all the front end being built on top of the Bootstrap grid. While it is also possible to style the entire front-end using original CSS, Bootstrap was used to quickly put in components and cut down on styling work.

Bootstrap is already installed under the `uibuilder\virt-tool\src\bootstrap` directory.

The front end also leverages some of Bootstrap's javascript features such as the modals (mostly seen as the "loading" pop up). **Note:** there is a bug with Bootstraps collapsible components, though all parts of the current front-end can operate fine without it.

[To learn more, view the Bootstrap documentation here.](https://getbootstrap.com/docs/4.3/getting-started/introduction/)

## JQuery

JQuery is a requirement for Bootstrap and some parts of the uibuilder node, and is a popular javascript library used throughout the web. Parts of JQuery is used is some parts of the front-end scripts. The current version of JQuery used in the front end is supplied through the uibuilder node.

More information about JQuery can be found [here.](https://jquery.com/)

## D3

D3.js is the javascript library used to create all the graphs and visualizations in the front-end. That includes the graph on the filter screen, the timeline in the video edit screen, and how drawing the bounding boxes on the video is done.

D3.js and JQuery have similar features when it comes to selecting HTML elements (called the DOM), and both selecting methods are used interchangeably throughout the front-end code. While learning D3 can be difficult, it can be flexible to do a wide range of tasks.

[To learn more about D3, visit here.](https://d3js.org/)
