# Front End Pages - Filter

The filter page is where the user can look through the database and find videos that match a search criteria. This logic is contained within `filter-logic.js`. The script receives no information from the back end, but instead reads the files that it produces.

The backend produces a file called `FilterData.txt` which contains information about all the video files in JSON format. The script loads the data from the file into a variable and works from there.

Since there is no date information within the dataset, the dates for the videos are set at a random hour starting on Aug 11 2019 and 36 hours after midnight of Aug 11 2019. This can be changed with `randDatestampStart` and `randHourRange` in `filter-logic.js`.

## Filter Options

The left side of the page has a form with inputs for dates and the various events and objects. The checkboxes are Bootstrap's unique styled checkboxes and are hard coded into the HTML, an improvement would be to generate the checkboxes through javascript.

Once the filter button is pressed, `filter-logic.js` grabs the form values and performs a search through the video file information. If there are results, it will give the correct information to the Result Visualizer, to the back end for the Map, and display the results below in the rest of the colum. If the filter query is invalid, it will display error messages where the video results would be to the user.

Currently the filter returns empty if there is no time supplied, and does not check if the start date is after the end date and will returns empty. Both can be changed in the future if needed.

Once the user clicks on a filter results, the page navigates to `video.html`.

## Map Screen

As explained in [the front end setup](front-end-setup.md) doc, the map shown here is driven by the web-worldmap node in an iframe. The front end sends the back end a list of file names to display on the map.

The map locations are randomized.

## Result Visualizer

The result visualizer is a graph that is drawn in D3.js and displays the videos, objects, and events that appear between the user selected dates. Though technically three seperate graphs, it is all drawn as one graph on the page with the legend and tooltips being HTML elements that float over it. Most of the logic is in `vis-graph.js`, and the graph is only drawn once a query with data to drawn is made.

Once `filter-logic.js` finds video files that matches the user filter query, it sends all data (objects, events, etc) to `vis-graph.js` through the `DisplayVisualiser()` function inside `vis-graph.js`. The data is then broken down even further into data that can be easily be displayed with D3.js, where it is basically displayed as 2 stacked graphs. More detail on how this is done can be found within `vis-graph.js` itself.

Currently, there is no way that the front or back end can find the length of the video files at this stage. The code to display the video files remains but unused, and can be added back in once that feature is been made complete.

The graph is cleared every time the user sends a query, since there is no graceful way to animate and redraw the graph with new data. The axis gridlines are not dynamic based on the filter dates, and instead are drawn at hour increments and this makes the visuals incohesive between different spans of time. It is possible to fix these flaws within D3.js, however that will require more time spent to researching the necessary components of the D3.js library.
