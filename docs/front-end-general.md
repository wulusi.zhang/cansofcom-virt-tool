# Front End - General Components

## Debug Console

The debug console uses a modal that displays all information that the uibuilder passes through, it contains the basic functionality on how to get all information that the front end can receive.

## Loading Modal

The loading modal is an element that appears that blocks the user from using the tool, its mostly used when the back end is crunching data. If the user attempts to navigate through the front end while the back end is doing a heavy load, the front end will freeze.

## SASS Files

The following lists how the `.scss` files are used to style the front end.

Filename | Purpose
--- | ---
`main.scss` | When compiling the `.scss` files to `.css` we can compile just this file. This file imports all other `.scss` files.
`_variables.scss` | Common variables used throughout the styling, contains the colour pallet used for the front end
`_base.scss` | Basic styling to the HTML thats consistent across all web pages
`_debug-console.scss` | Styling done to the debug console
`_filter.scss` | Styling done to the `filter.html` page. Contains styling for the visualizer as well.
`_graph.scss` | Contains variables and styling used across all D3 graphs, mainly the colours used to display objects and events.
`_login.scss` | Styling done to `login.html`, currently there is none.
`_mods.scss` | Modifications made to Bootstrap components, specifically overrides the classes it uses for its styling
`_session.scss` | Styling done to `session.html`
`_video.scss` | Styling done to `video.html`, contains styling for bounding boxes and transport controls.
